---
title: k8s上日志收集方案
date: 2019/7/13 20:46:25
description: 就k8s上如何收集日志进行简单梳理
categories:
- 云原生专题
tags:
- k8s
- docker
---
由于项目目前慢慢推进容器化部署，所以就k8s上如何收集日志进行简单梳理
## k8s日志归集方案   
其实官网已经有推荐  [中文连接](https://k8smeetup.github.io/docs/concepts/cluster-administration/logging/)
这里做一个简单的总结  

|方案|优点|缺点|
|---|---|---|
|容器级别推送日志|部署方便，k8s部署文件不用特殊配置|日志收集组件和应用强耦合dockerFile相对复杂|
|pod级别推送日志|耦合度较低，扩展性强，可随时替换组件|k8s的yaml文件需特殊配置，每个pod都会运行收集组件，占用较大资源|
|节点级别推送日志|耦合度低，且可适应多个项目，有项目上载不用增加开销|需要统一日志规范，比如每个日志建立统一标识|

在蔡神的建议下我们选择使用节点级别推送

## 使用节点级日志代理

    
![Alt 仓库概念图](https://upload-images.jianshu.io/upload_images/23765437-5b7b67b4258aa9a2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 应用日志

k8s只是管理容器的一个框架，所以应用日志就和docker日志一样，docker会把标准输出的日志
重定向到某个位置`/var/lib/docker/containers/<容器id>/<容器id>-json.log`。  
因此我们可以使用收集组件对节点上所有日志进行统一收集。  

对于 k8s集群俩说是最常用和被推荐的方式，因为在每个节点上仅创建一个代理（收集器），并且对集群上的应用毫无感知。不过节点级的日志 仅适用于应用程序的标准输出和标准错误输出。

### 收集方案

对于节点收集方案有两种可选，一种是本地机器启动后台进程，另一个是在k8s上部署[DaemonSet](https://k8smeetup.github.io/docs/concepts/workloads/controllers/daemonset/)守护进程，第二种是推荐的做法。

+ 收集器的选择：  
    通常有两种fileBeat或者flentd，差别不太，因为之前整个日志归集方案是ELK全家桶，所以我们这里使用FileBeat, fileBeat中有专门收集docker日志的配置，这里不做详述。

+ [X] 其他问题：  
    因为kubernetes不负责日志的轮转，所以我们得替代方案，不过docker有自己的日志方案，所以我们采用配置docker日志的方式，如何配置后面再分析。    