---
title: k8s、docker等扫盲
date: 2019/7/13 20:46:25
description: 了解docker、k8s等一些云原生的概念，以及他们之间的关系
categories:
- 云原生专题
tags:
- k8s
- docker
---
### 导读： 
+ [docker是什么](#docker是什么)  
+ [k8s介绍](#k8s是什么)  

# docker是什么
要了解docker是什么，我们先了解下虚拟机和容器  
## 容器和虚拟机技术
玩过虚拟机的肯定知道，VMWare和OpenStack，他们的作用都是让你在一个操作系统里模拟出多个电脑（可以不同系统）  
虚拟机技术让我们可以模拟不同的系统，但是用过的人都知道，装VMWare比较麻烦（尤其要装免费的），也比较吃资源。   

怎么办呢？
容器技术应运而生（当然[起源](http://dockone.io/article/8832)比较复杂，这里不赘述）  
与VMWare相对，容器技术是一个**轻量级**的虚拟化，例如我们同一台电脑运行多个Nginx，多个qq，相互隔离。  
下面用一张图来看他们之间的区别。  
![image (1).png](/images/k8s/k8s_VM.jpg)
***划重点***   
>虚拟机有自己的操作系统，而容器技术没有，是利用的宿主机资源，虽然相对安全程度和隔离级别都会差一点但它能更小、更快、资源利用率更高。  


### 那什么叫轻量级的虚拟化？
打个比方
> 有个人比较傲娇，出差的时候，床要有:
> 1. 专门的被子
> 2. 专门的枕头  
说白了无论哪个酒店都要定制一张床。 
 
+ 如果这个人把它整个家搬迁，或者整个卧室打包携带，我们肯定会吐槽太麻烦，明明只需要一张合适的床就行
+ 但是每到一个地方就重新选购被子、枕头等也太麻烦了。   
于是我们就想:  
  
**`能不能只打包一张床,到哪它都有一整套，又好又快?`**  

容器就是那张床！这里虚拟化就像整个卧室、电脑就像整个房子。   

![image (2).png](/images/k8s/docker.jpg)
> 我们部署一个web运用
> 1. 需要jre1.8环境
> 2. 需要Nginx反向代理静态文件

像上面的例子不要求房间、不要求冰箱、不要求厕所等，只要有这个床他就干活，
我们的应用不要求操作系统、不要求有否python、给我一个jre1.8和Nginx，它就能跑。 
再回头看上面第一张图，容器技术就是这样一种轻量级级的虚拟化，它把所需要的包括进去。不需要的就利用宿主机自带的。  

而docker就负责打包那张床  
***Build once，Run anywhere***
## Docker
再回到docker，**docker本身不是容器！**  
它只是一个工具，来创建容器，就像VMWare一样。或者说像我们常用的maven、npm一样。  
>对于java应用，首先编写java代码  ->  package -> deploy -> pom引用，就变成了我们的。  
>docker一样，也是写代码（dockerfile）、编译镜像、发布镜像、运行镜像，就变成了容器。
 
回到上面的例子，docker所做的工作就是构造床（引入jre、Nginx）、编译成镜像（打包成一个整体，可以看成是床的模板）、发布（将镜像推送到仓库） 
最后我们无论到哪里，就可以直接部署，因为仓库已经有这个镜像了（这张床模板）

![image.png](https://upload-images.jianshu.io/upload_images/23765437-a6e9b295f8a58c54.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

docker一般工作如下：
1. docker build -t {imageName} .
  >t参数，也就是标签名，就是给这个镜像起个名字。然后`.`代表在当前目录编译，前提是当前目录有DockerFile，也可以自己指定Dockerfile
2. dodcker push {imageName}
  >将编译好的镜像推送到docker仓库，可以配置私有仓库或者默认公共仓库
3. docker pull {imageName}
  >从仓库中拉取镜像
4. docker run {imageName}
  >运行镜像，运行完成之后使用docker ps命令就可以看到运行的镜像实例，也就是容器container了。

参考文章：   
[Docker 不同场景编写案例]
[DockerFile编写规范]
# k8s是什么  
# gitlab是什么
# Jenkins是什么
# PaaS是什么