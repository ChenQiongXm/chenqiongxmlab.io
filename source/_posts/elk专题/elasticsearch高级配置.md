---
title: elasticsearch高级配置
date: 2020-10-16 09:00:39
categories: elk
tags: 
    - 配置
    - elasticsearch
description:  想要部署一个真正实用的es集群，一些系统相关的配置必不可少
---
大多数设置可以使用 [集群更新API](https://www.elastic.co/guide/en/elasticsearch/reference/6.5/cluster-update-settings.html)来更改

Elasticsearch提供了三个主要的配置文件，我们所有的配置都通过这个三个文件：
+ Elasticsearch配置文件：elasticsearch.yml  
+ jvm配置文件：jvm.options  
+ 日志配置文件：log4j2.properties  

## 1、设置JVM选项
您应该很少需要更改Java虚拟机（JVM）选项。如果这样做，最可能的更改是设置堆大小。

+ <font color="501818">**堆大小**</font>

    默认情况下，Elasticsearch告诉JVM使用最小和最大大小为1 GB的堆。迁移到生产环境时，配置堆大小以确保	Elasticsearch有足够的堆可用是很重要的。

    1、将最小堆大小（Xms）和最大堆大小（Xmx）设置为彼此相等  

    2、lasticsearch可用的堆越多，它可用于缓存的内存就越多。但请注意，过多的堆可能会使您陷入长时间的垃圾收集暂停。  

    3、设置Xmx为不超过物理RAM的50％，以确保有足够的物理RAM留给内核文件系统缓存。  

    4、不要设置Xmx为JVM用于压缩对象指针（压缩oops）的截止值以上; 确切的截止值变化接近32 GB，大多数是26GB。  
    
    **设置方式**  
    
    + 修改jvm.option 

        -Xms2g  
        -Xmx2g  
    + 在启动elasticsearch时加入参数  

        ES_JAVA_OPTS="-Xms2g -Xmx2g" ./bin/elasticsearch   
        ES_JAVA_OPTS="-Xms4000m -Xmx4000m" ./bin/elasticsearch    

<font color="501818">**10. 堆转储路径**</font>

默认情况，配置jvm存储堆栈溢出到data文件夹里，如果该目录不支持则需要修改。

<font color="501818">**11. GC记录**</font>  

默认情况下，Elasticsearch启用GC日志。这些配置在 jvm.options默认位置和默认位置与Elasticsearch日志相	同。默认	配置每64 MB轮换一次日志，最多可占用2 GB的磁盘空间。
## 2、安全设置：
某些设置是敏感的，依靠文件系统权限来保护其值是不够的。对于此用例，Elasticsearch提供了一个密钥库和elasticsearch-keystore管理密钥库中设置的工具。

## 3、elasticsearch.yml设置
在投入生产之前，必须考虑以下设置：

<font color="501818">**1.路径设置**</font>   
主要是数据和日志路径
```yaml
path:  
    logs: /var/log/elasticsearch  
    data: /var/data/elasticsearch  
```
对于数据，可以有多个路径如：
```yaml
path:
    data:
    - /mnt/elasticsearch_1
    - /mnt/elasticsearch_2
    - /mnt/elasticsearch_3
```
data中生成目录形式：elasticsearch/nodes/0/indices/uuid/shard/  
    
+ indices下是以各个索引的uuid命名的文件夹     
+ shard,同一个分片会被分配到同一个目录   

<font color="501818">**2. 群集名称**</font>   
    这是节点加入集群的唯一方法，默认的是elasticsearch  

<font color="501818">**3. 节点名称**</font>   
    节点名称有助于区别不同类型(可以使用环境变量如 `node.name: ${HOSTNAME}`)  
<font color="501818">**4. 网络主机 network.host**</font>     
    一般设置本机ip ，一旦设置为非回环地址，即默认视为生产模式，就需要注意系统配置  
一旦配置了类似的网络设置network.host，Elasticsearch就会假定您正在转向生产并将上述警告升级为异常。这些异常将阻止您的Elasticsearch节点启动。这是一项重要的安全措施，可确保您不会因服务器配置错误而丢失数据。  
<font color="501818">**5. 发现设置**</font>  
```yaml
discovery.zen.ping.unicast.hosts：
-  192.168.1.10:9300
-  192.168.1.11 ①
-  seeds.mydomain.com   ②
```
①：如果不指定port，则会使用transport.tcp.port  
②：如果是多个ip的主机，则会访问所有解析到的ip  
discovery.zen.minimum_master_nodes
最好为 （master_eligible_nodes / 2）+ 1  3个节点的情况设置2个  

## 3、系统设置
<font color="501818">**1. 资源限制：**</font>  
要将打开文件句柄（ulimit -n）的数量设置为65,536  /etc/security/limits.conf

<font color="501818">**2. Disable swapping**</font>  

交换对性能，节点稳定性非常不利，应该不惜一切代价避免。它可能导致垃圾收集持续数分钟而不是毫秒，并且可能导致节点响应缓慢甚至断开与群集的连接。在弹性分布式系统中，让操作系统终止节点更有效。
三种方案：  
+ 禁用所有的交换   临时：sudo  swapoff -a  永久修改： 在/etc/fstab  注释掉所有包含swap的行
+ 配置swappiness    
设置  sysctl 中 vm.swappiness =1 这降低了内核交换的倾向，在正常情况下不应导致交换，同时仍允许整个系统在紧急情况下交换。
+ 启用bootstrap.memory_lock  
修改 config/elasticsearch.yml：bootstrap.memory_lock: true  

<font color="501818">**3. Increase file descriptors**</font> 

    Elasticsearch使用大量文件描述符或文件句柄。用完文件描述符可能是灾难性的，最有可能导致数据丢失。确保将运行	Elasticsearch的用户的打开文件描述符数量限制增加到65,536或更高。
    /etc/security/limits.conf  
    
<font color="501818">**4. Ensure sufficient virtual memory**</font>  

    可以设置存储方式（fs、simplefs、niofs、mmapfs），默认Elasticsearch会选择最佳的存储方式，
    Elasticsearch 默认使用mmapfs文件夹存储索引 ，操作系统mmap计数的默认限制可能太低，这可能导致内存不足异常。  
    临时修改： sysctl -w vm.max_map_count = 262144  
    永久修改：/etc/sysctl.conf  
<font color="501818">**5. Ensure sufficient threads**</font>  

    Elasticsearch为不同类型的操作使用了许多线程池。重要的是它能够在需要时创建新线程。确保Elasticsearch用户可以创建的线程数至少为4096。  
    临时： ulimit -u 4096  
    永久： /etc/security/limits.conf.  
<font color="501818">**6. JVM DNS cache settings**</font>  
<font color="501818">**7. Temporary directory not mounted with noexec**</font>  

 
 
<font color="501818">**8. 临时目录**</font>  

	默认情况，Elasticsearch 使用一个私有的临时文件夹来存储历史脚本  
	在一些linux版本中，会自动清除在一段时间内没有被访问到的文件，所以对于Elasticsearch来说是致命的
	如果安装.tar.gz在Linux 上运行分发一段时间，那么您应该考虑为Elasticsearch创建一个专用的临时目录，该目录不在	将从中清除旧文件和目录的路径下。此目录应具有权限集，以便只有运行Elasticsearch的用户才能访问它。然后	$ES_TMPDIR在启动Elasticsearch之前将环境变量设置 为指向它。


## 3、安全设置

Generate a private key and X.509 certificate.
生成节点证书

1. 创建一个一个整数颁发机构,命令 : `bin/elasticsearch-certutil ca`  
您可以将群集配置为信任具有此CA已签名的证书的所有节点。
这个命令生成一个单独的文件（默认名称是elastic-stack-ca.p12）该文件是PKCS＃12密钥库，包含CA的公共证书以及用于为每个节点签名证书的私钥。

2. 为群集中的每个节点生成证书和私钥。
    > bin/elasticsearch-certutil cert --ca elastic-stack-ca.p12

    输出是单个PKCS＃12密钥库，包括节点证书，节点密钥和CA证书。

3. 将节点证书复制到适当的位置。

4. 将elastic-stack-ca.p12和elastic-certificates.p12复制到其他节点

5. 配置elasticsearch.yml文件：

    >xpack.security.transport.ssl.enabled: true
    xpack.security.transport.ssl.verification_mode: certificate 
    xpack.security.transport.ssl.keystore.path: certs/elastic-certificates.p12 
    xpack.security.transport.ssl.truststore.path: certs/elastic-certificates.p12 


6. 设置密码 

7. 报错  
    ```json
    {
        "error": {
            "root_cause": [
                {
                    "type": "security_exception",
                    "reason": "current license is non-compliant for [security]",
                    "license.expired.feature": "security"
                }
            ],
            "type": "security_exception",
            "reason": "current license is non-compliant for [security]",
            "license.expired.feature": "security"
        },
        "status": 403
    }
    ```
<font color="blue">**结论：**</font> 开源基础版，无法使用安全功能