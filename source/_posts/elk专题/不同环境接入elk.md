---
title: 不同环境接入elk
date: 2019/7/13 20:46:25
description: elk是什么，我们怎么把项目对接elk？
categories:
- elk专题
tags:
- elk
---
### 目录:  
+ [ELK是什么](#一-ELK是什么)
+ [目前项目应用场景](#二-目前项目应用场景)
+ [开始使用](#三-开始使用)
   - [kibana介绍](#kibana介绍)
   - [定位本质](#如何定位)
   - [案例](#案例)
+ [常见问题](#四-常见问题)

# 一 ELK是什么

[参考资料](http://3ms.huawei.com/hi/group/2830503/wiki_5475713.html)  

[FileBeat](https://www.elastic.co/guide/en/beats/filebeat/7.3/index.html)  
[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)  
[Logstash](https://www.elastic.co/guide/en/logstash/current/index.html)  
[Kibana](https://www.elastic.co/guide/en/kibana/current/index.html)   

**总结一句话，这是一个归集系统，也就是把所有环境、所有机器、所有目录的日志都集中起来管理，因此定位日志的方式跟以前大不一样。最后呈现给我们的就如同一个大的数据库**  
![image.png](https://upload-images.jianshu.io/upload_images/23765437-be2dd8d221a41989.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
# 二 目前项目应用场景
1. .log 文件+elk  如aiom、tpcloud、onu、sg

2. k8s+elk 

# 三 开始使用
以下文档，建议打开[kibana界面](http://localhost:5601/app/kibana#/discover?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(info,level,threadId,traceId),filters:!(),index:ed102ba0-690e-11ea-884d-df21c032ebe6,interval:auto,query:(language:kuery,query:''),sort:!('@timestamp',desc)))跟随操作
## kibana介绍
因为最后呈现给我们的是kibana，也就是一个用户界面，所以前面其他elasticsearch、logstash等不需要掌握，只需掌握[kibana工具使用]()即可  

总结一句话：**可以傻瓜式点点点**
## 定位本质
如上面说到，最后呈现跟我们的就是一个大的数据库，那么中心思想就不难想象，就是使用一个个字段来进行搜索、过滤、排序、关联。
>Ps:听说厉害的人甚至都不需要界面化kibana来查，有兴趣可以[用原生elasticsearch来增删改查]()   

下面用一个案例来说明一下操作步骤

## 案例
   >:high\_brightness: 一种是传统的was方式，一种是正在改进的docker方式也就是k8s环境。两套环境都已完成日志的归集部署
   
  + 要点一：索引根据不同环境来命名如：  
     - 环境1为：`test-%{+YYYY.MM.dd}`，因此可以建立索引模式`test-*`后面的+YYYY.MM.dd代表日期，也就是每天一个索引，我们使用正则来创建索引模式，是因为方便夸日期进行搜索，下同。  
     - 环境2： `testdev-%{+YYYY.MM.dd}`, 因此可以建立索引模式`testdev-*`

   >>这里可能有些同学会疑惑，为什么选择两个不同的索引模式，为什么不用同一个呢？我之前也考虑过用同一个，也就是一个项目一个索引，但是发现不同环境有很多不同的字段，比如k8s中有namespace、pod这些概念，传统环境中有主机概念、文件路径概念。如果放到同一个索引里，对不同的数据都会产生多余的字段，因为同一个索引是共享字段的。所以为了处理方便采取分离的方式。

  + 要点二：项目日志数据结构为:  
`%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level-***%X{traceId}***- [%thread] %logger{5} - %msg%n}`  
    此结构在elk已经转化为：
      + **time**  时间戳
      + **level** 日志级别
      + **traceId** 调用链traceId
      + **logger** 打印日志类
      + **info** 日志具体内容  
    
   + 要点三：模块 包括 portal、order、bpm等  

:question: ***知道这些有什么用呢？***  

记得上面说过的大的数据库吗，所有的数据都转化为一条sql记录，没错，一个索引就是一张表，以上这些统统都转化为一个个字段，如下表：
这些字段如何在elk上体现如下表：  

|字段名|环境1|环境2|备注|  
|---|---|---|---|
|模块如:order|moduleName|kubernetes.labels.app|  
|日志级别如:Error|level|level||
|时间戳|@timestamp|@timestamp|一般统计时间段或者排序用的上|  
|调用链|traceId|traceId|组合查询最重要的标识|
|日志所在类|logger|logger|如c.h.e.p.u.a.f.UeeUrlFilter|
|进程ID|threadId|threadId|threadId||
|日志内容|info|info|核心日志内容|
|完整日志|message|message|一条日志的所有内容，其他多个字段由它拆分而来|
|日志路径|log.file.path|不需要||
|主机名|||暂时不用，后期加上|

（在kibana Discovery界面的左侧部分参考本文最后一张图）

时间戳、日志级别、调用链traceId等一个都不能少。如果现在对照着kibana你是不是发现还有些奇怪的字段，有兴趣的可以去了解，影响不大。

其他的字段有兴趣的同学可以去探索  
 + :mrs\_claus: **真实场景一**    
4. 图形化显示  
    [参考资料]()

# 四 常见问题
1. 没有日志   
   ①、确定elasticsearch组件是否正常 (get 请求)   
    > `curl http://localhost:9200/_cat/health?v`  
        主要观察status是否为green  

   ②、确定当天日志索引是否存在    
   >`http://localhost:9200/_cat/indices?v`  
      或者  
    `GET /_cat/indices/<index>` 
 
     全索引查询，查看结果中是否有对应的日志索引  
 
    以上如果都正常，那么可能就是你限定条件错误，导致查询失败。如果异常，那么确实是没有收集到。一般步骤(查看kafka中topic  -> 查看logstash日志 -> 查看源日志)  

2. 无法搜索到日志     
   ①、确定搜索条件是否正确（模块、时间、关键字）  
   ②、可能需要等待几分钟（还没收集到，延迟或者磁盘满了）
   ③、时区是否有问题，尤其是针对8点以前的日志，解决方法是往前搜索一天。[此问题已解决]()
3. 日志时间有问题  
   有两个时间。  
   ①、k8s中容器时区不对一般差8个小时,参考上面2.3     
   ②、日志打印时间和收集时间不一致，一般差1秒以内
4. kibana 不可用的怎么办  
    > [使用原生elasticsearch的restful请求]()  