---
title: Elasticsearch基础之基本概念
date: 2019/7/13 20:46:25
description:  Elasticsearch
categories:
- elk专题
tags:
- Elasticsearch
---
# 目录
+ [概念](#基本概念)
+ [安装](#安装)
+ [使用](#使用)

## 基本概念

<font color="084B8A">1. Near Realtime（NRT）：</font>  
    
 Elasticsearch是一个近实时搜索平台。 这意味着从索引文档到可搜索文档的时间有一点延迟（通常是一秒）.

<font color="084B8A">2. Cluster：</font>  

 集群是一个或多个节点（服务器）的集合，它们共同保存您的整个数据，并提供跨所有节点的联合索引和搜索功能。 群集由唯一名称标识，默认情况下为“elasticsearch”。 此名称很重要，因为如果节点设置为按名称加入群集，则该节点只能是群集的一部分。  

 >*<font color="084B8A">  确保不要在不同的环境中重用相同的群集名称，否则最终会导致节点加入错误的群集。 例如，您可以将logging-dev，logging-stage和logging-prod用于开发，登台和生产集群。请注意，如果集群中只有一个节点，那么它是完全正常的。 此外，您还可以拥有多个独立的集群，每个集群都有自己唯一的集群名称。</font>*

<font color="084B8A">3. Node:   </font>

节点是作为群集一部分的单个服务器，存储数据并参与群集的索引和搜索功能。就像集群一样，节点由名称标识，默认情况下，该名称是在启动时分配给节点的随机通用唯一标识符（UUID）。如果不需要默认值，可以定义所需的任何节点名称。此名称对于管理目的非常重要，您可以在其中识别网络中哪些服务器与Elasticsearch集群中的哪些节点相对应。

>*<font color=grey>可以将节点配置为按群集名称加入特定群集。默认情况下，每个节点都设置为加入名为elasticsearch的群集，这意味着如果您在网络上启动了许多节点并且 - 假设他们可以相互发现 - 他们将自动形成并加入名为elasticsearch的单个群集。*

在单个群集中，您可以拥有任意数量的节点。此外，如果您的网络上当前没有其他Elasticsearch节点正在运行，则默认情况下，启动单个节点将形成名为elasticsearch的新单节点集群。</font>

<font color="084B8A">4. Index:</font>  

索引是具有某些类似特征的文档集合。 例如，您可以拥有客户数据的索引，产品目录的另一个索引以及订单数据的另一个索引。 索引由名称标识（必须全部小写），此名称用于在对其中的文档执行索引，搜索，更新和删除操作时引用索引。
在单个群集中，您可以根据需要定义任意数量的索引.  

<font color="084B8A">5. Type  （在6.0.0中弃用）</font>  

一种类型，曾经是索引的逻辑类别/分区，允许您在同一索引中存储不同类型的文档，例如 一种用户类型，另一种用于博客帖子。 不再可能在索引中创建多个类型，并且将在更高版本中删除类型的整个概念。 请参阅删除映射类型以获取更多信息。  

<font color="084B8A">6. Document</font>

文档是可以被索引的基本信息单元。 例如，您可以为单个客户提供文档，为单个产品提供另一个文档，为单个订单提供另一个文档。 该文档以JSON（JavaScript Object Notation）表示，JSON是一种普遍存在的互联网数据交换格式。

>*<font color=grey> 在索引/类型中，您可以根据需要存储任意数量的文档。 请注意，尽管文档实际上驻留在索引中，但实际上必须将文档编入索引/分配给索引中的类型。*</font>

<font color="084B8A">7. Shards & Replicas</font>

索引可能存储大量可能超过单个节点的硬件限制的数据。 例如，占用1TB磁盘空间的十亿个文档的单个索引可能不适合单个节点的磁盘，或者可能太慢而无法单独从单个节点提供搜索请求。
为了解决这个问题，Elasticsearch提供了将索引细分为多个称为分片的功能。 创建索引时，只需定义所需的分片数即可。 每个分片本身都是一个功能齐全且独立的“索引”，可以托管在集群中的任何节点上。
>*<font color=grey> 分片很重要，主要有两个原因：*  
>+ *它允许您水平拆分/缩放内容*
>+ *它允许您跨分片（可能在多个节点上）分布和并行化操作，从而提高性能/吞吐量*</font>

总而言之，每个索引可以拆分为多个分片。 索引也可以复制为零（表示没有副本）或更多次。 复制后，每个索引都将具有主分片（从中复制的原始分片）和副本分片（主分片的副本）。

><font color=grey> *可以在创建索引时为每个索引定义分片和副本的数量。 创建索引后，您还可以随时动态更改副本数。 您可以使用_shrink和_split API更改现有索引的分片数，但这不是一项简单的任务，预先计划正确数量的分片是最佳方法。*</font>

默认情况下，Elasticsearch中的每个索引都分配了5个主分片和1个副本，这意味着如果群集中至少有两个节点，则索引将包含5个主分片和另外5个副本分片（1个完整副本），总计为 每个索引10个分片。

<font color="B18904"> note:**每个Elasticsearch分片都是Lucene索引。 单个Lucene索引中可以包含最大数量的文档。 自LUCENE-5843起，限制为2,147,483,519（= Integer.MAX_VALUE  -  128）个文件。 您可以使用_cat / shards API监视分片大小。**</font>

## 安装

### 1、下载zip/tar.gz文件

### 2、配置管理工具安装（可选）
+ Puppet
+ Chef
+ Ansible

### 3、运行

   控制台运行 `./bin/elasticsearch`
   作为后台程序运行 `./bin/elasticsearch -d -p pid`

### 4、简单配置
配置文件：config/elasticsearch.yml   
任何能使用配置文件配置的都可以通过命令行来配置，但是集群配置需要通过配置文件配置，节点配置则可以通过命令行。


### 5、目录结构

文件夹 | 内容
---- | ---
bin | 脚本
conf|配置
data |索引和分片的数据存放，可以使用多个location  path.data
logs  |  日志   path.logs
plugins|  插件
repo   | 共享文件系统存储库位置。可以容纳多个位置。文件系统存储库可以放在此处指定的任何目录的任何子目录中。  path.repo 
script| 脚本
- - -
## 使用
### 索引和查询

1、两种查询方式  
+ 使用纯粹的get url方式
如：  
get http://ip1:9200/bank/_search?q=*&sort=account_number:asc&pretty

+ 使用body的方式：  
    ```
    post： http://ip1:9200/bank/_search?pretty  
    Body：
    {
    "query": { "match_all": {} },
    "sort": [
        { "account_number": "asc" }
    ]
    }
    ```

2、查询范围

- query：
- size：
- from：
- sort：

3、查询返回字段：_source

- 一般我们查询，都会返回该数据的所有 ，倘若只需要查询的部分数据则可以指定_source
```GET /bank/_search
{
  "query": { "match_all": {} },
  "_source": ["account_number", "balance"]
}
```

4、匹配指定条件

```
GET /bank/_search
{
  "query": { "match": { "account_number": 20 } }
}
```

5、bool查询
+ 与  
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
+ 或  
```
GET /bank/_search
{
  "query": {
    "bool": {
      "should": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
+ 非  
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must_not": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
6、使用过滤器查询  
eg 范围查询

7、聚合操作  

在Elasticsearch中，可以执行返回匹配的搜索，同时在一个响应中返回与命中相关的聚合结果。而不用执行多次

+ 统计   group by  
    ```
    GET /bank/_search
    {
    "size": 0,
    "aggs": {
        "group_by_state": {
        "terms": {
            "field": "state.keyword"
        }
        }
    }
    }
    ```
    等同于数据库中   
`SELECT state, COUNT(*) FROM bank GROUP BY state ORDER BY COUNT(*) DESC LIMIT 10;`

+ 计算（平均值）
    ```
    GET /bank/_search
    {
    "size": 0,
    "aggs": {
        "group_by_state": {
        "terms": {
            "field": "state.keyword",
            "order": {
            "average_balance": "desc"
            }
        },
        "aggs": {
            "average_balance": {
            "avg": {
                "field": "balance"
            }
            }
        }
        }
    }
    }
    ```


### 增删改


### 集群状态
1、基本状态查看  

http://ip1:9200/_cat/health?v
```
epoch      timestamp cluster     status node.total node.data shards pri relo init unassign pending_tasks max_task_wait_time active_shards_percent

1564467233 06:13:53  elastic_green           3         3    116  58    0    0        0             0                  -                100.0%
```
**status：** 
  + <font color="11BB">Green</font> ： 集群健康
  + <font color="FFaa00">Yellow</font>： 数据正常，但是有些副本还没有分配
  + <font color=red>Red</font>：数据丢失，不可用


2、获取所有的节点信息  

http://ip1:9200/_cat/nodes?v
```
ip            heap.percent ram.percent cpu load_1m load_5m load_15m node.role master name
ip1           59          73   0    0.03    0.02     0.00 mdi       *      node-2
ip2           32          94   1    0.05    0.03     0.06 mdi       -      node-3
ip3            65          71   0    0.00    0.00     0.00 mdi       -      node-1
```
3、获取所有的索引  

http://ip1:9200/_cat/indices?v