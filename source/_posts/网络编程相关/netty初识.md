---
title: netty初识
date: 2019/7/13 20:46:25
categories:
- 网络编程
tags:
- netty
---
为了学习netty，之前花了一段时间读完《NIO与Socket编程技术指南》收获一般，基本就是一些api的翻译，因此本该做一些nio相关的文章总结迟迟没有下手。待后续看到更好的资料或者看完netty后是否有更好的总结。
<!--more-->
- - -
# 为什么使用netty？
网上有一堆的文章，我就简短的总结下：
1、性能好甚至比java的核心API有更好的吞吐量和更低的延迟
2、易于使用、统一的api
3、开源.
....总之就是好处多多哈哈。
>下面这张图估计一开始看不太懂，我也没太看懂，没事，慢慢往下看，时不时回来看这张图。  
![netty架构](https://upload-images.jianshu.io/upload_images/23765437-d9acf971105b8cee.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 丰富的缓存数据结构 ByteBuf
看到图中的最下方 `ByteBuf`说的就是这家伙，在4.0版本之前叫做ChannelBuffer 。
相对于NIO中的ByteBuffer， [`ByteBuf`]()是一个完全重新制作的一个类，在网络编程中更有优势。
+ 通过复合缓冲区达到0拷贝
+ 可以自定义缓冲类型
+ 开箱即用的动态缓冲区类型，可以像StringBuffer一样按需扩展。
+ 不再需要使用烦人的flip()函数了（尤其是对于我这种对于rewind()、flip()、clear()老是分不清的来说算是一种福音）
+ 比ByteBuffer更快（不太明白为什么单独说这条，按理说跟第一条重复了，[后面再看]()）

## 通用的异步IO接口
上图中的倒数第二行(`Universal Communication API`)
传统的I/O接口，对于Socket、DatagramSocket没有统一起来，这种模式的缺点是不便于移植，而且NIO、NIO.2(AIO)接口又不一样。netty就不一样了，提供了统一的接口
## 基于拦截链模式的事件模型
上图中的倒数第三行(`Extensible Event Model`)
Netty有一个针对I / O的定义明确的事件模型
这个地方在4.X版本有所改造，后续补充
## 其他组件
比如编码器框架、http实现等这些组件会使得我们的开发更加快捷

# 核心组件
1. Channel
2. 回调
3. Future
4. 事件和ChannelHandler
![截图.PNG](https://upload-images.jianshu.io/upload_images/23765437-7976e17cc4c3bfff.PNG?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
