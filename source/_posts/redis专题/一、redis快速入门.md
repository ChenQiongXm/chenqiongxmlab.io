---
title: 一、redis快速入门
date: 2020-10-17 19:00:39
categories: 
	- redis专题
description:  redis基础介绍，包括概念以及使用方法
---
## redis特性

1. 速度快，使用内存存放数据，且c语言实现，速度可达10万/s
2. 单线程架构，避免多线程的竞争问题
    + 纯内存访问，无需多线程已足够快
    + 非阻塞I/O ，采用的是epoll模式
    + 避免多线程中的线程切换，以及竞争带来的开销
    + 多个命令会串行执行，因此每个命令必须不能过长，不然会阻塞其他命令，比如生产上禁止使用`keys *` 这样的命令。
3. 基于键值对的数据机构
4. 其他功能，如过期、发布订阅、事务等
5. 持久化，redis采用两种方式持久化，RDB和AOF
6. 支持主从复制、高可用和分布式

## 使用场景

1. 缓存
2. 排行榜系统
3. 计数器
4. 消息队列

## 安装启动

参考[Linux下redis安装和部署](https://www.jianshu.com/p/bc84b2b71c1c)

1. 下载源码并解压（一共就2M）
2. 编译

    ```sh
    cd redis
    make
    ```

    注意，系统中需要有gcc的编译器，如果编译报错，大多是因为gcc版本的问题。  
3. 启动，编译完成后，可直接在src文件夹下执行命令，或者通过make install命令安装。
4. 关闭，src/redis-cli shutdown
5. 远程访问，远程访问需要重新设置bind 参数，默认是bind 127.0.0.1,需要修改对应ip，或注释掉，另外需要修改 protect-mode 属性为no

## 常用命令

### 全局命令

+ keys pattern: 查看所有键,使用正则
+ dbsize: 获取键总数
+ exists key : 检查键是否存在
+ del key : 删除键
+ expire key seconds : 设置键过期
+ type key: 获取键结构类型
+ object encoding key : 获取键底层编码
+ rename key newkey: 键重命名
+ randomkey : 随机返回一个key
+ move key db3: 同个实例不同库迁移键
+ dump+restore|migrate：不同实例间迁移

## redis为什么不适用多数据库

在旧版本中，有多数据库的功能，从0到15，默认使用的是0 ，可以使用select [idx] 来切换，但新版本逐渐弱化了这个功能。
原因还是因为redis是单线程的，如果使用多数据库，那么这些数据库在执行时还是会互相影响

## 五种基本数据结构

参考[官方文档](http://www.redis.cn/commands.html#generic)

## scan

其他指令比较容易理解，就不单独展开讲了,重点介绍一下scan指令。  
之前我们如果有需要遍历键的时候需要用到scan pattern命令，这个命令有个致命的缺点，它会找到所有匹配的值，如果匹配到的数据量过大，不但输出可能爆炸，而且会阻塞其他操作。
于是Redis从2.8版本之后，提供了一个新的命令scan，它能有效的解决上面说的问题。

+ scan命令是什么
scan命令非常类似java中的迭代器，每次都是逐渐的去遍历列表，也就是每次只读一部分数据。不同的地方是，迭代器每次返回一个，但scan可以返回多个。
+ scan 操作方式

    ```sh
    #SCAN cursor [MATCH pattern] [COUNT count]
    # 第一次遍历，从0开始
    127.0.0.1:6379> scan 0 match user* 
    1) "14"
    2) 1) "user"
       2) "user:a:follow"
    # 第二次遍历从上次返回的游标开始
    127.0.0.1:6379> scan 14 match user* count 10
    1) "0"
    2) 1) "user:2:follow"
       2) "user:ranking"
       3) "user:1:follow"
    ```

    说明：

+ 提供三个参数，第一个是cursor游标，第二个是key的正则匹配表达式，第三个是遍历的最大值
+ limit 不是限定返回结果的数量，而是限定服务器单次遍历的字典槽位数量(约等于)，从上面的过程可以看到虽然提供的 limit 是 10，但是返回的结果只有 3 个左右。
+ 返回值包含两部分，第一部分是一个整数，代表下一个游标值，第二部分是本次遍历的结果
+ 只有游标值返回为0的时候才是遍历结束
