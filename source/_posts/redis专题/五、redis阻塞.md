---
title: 五、redis阻塞
date: 2020-11-22 19:55:32
categories: redis专题
tags: 
    - 阻塞
description: redis是典型的单线程架构，所有的读写都在一个线程上执行，如果发生阻塞，将会是噩梦
---

## 发现阻塞

当redis阻塞时，最先感知到的应该是应用客户端，可以借助日志统计发现

## 内在原因

### 数据或者调用api不合理

如果一个redis的值过大，在操作时会非常慢，比如一个包含上万个元素的hash结构，执行hgetall操作。  

1. 如何发现慢查询  
Redis原生提供慢查询的功能，使用slowlog get{n}命令，查看最近的n条慢查询记录。发现慢查询后，开发人员需要及时优化
    + 修改为低时间复杂度的算法
    + 调整大对象

2. 如何发现大对象  
Redis本身提供发现大对象的工具，对应redis-cli bigkeys，

### CPU饱和
redis处理命令时只使用一个cpu，当cpu资源不足时，将会发生阻塞

### 持久化阻塞

一般我们都会开启持久化功能，当redis发生持久化操作时，可能导致阻塞，如：fork阻塞、AOF刷盘阻塞、HugePage写操作阻塞

1. fork阻塞  
RDB和AOF重写(RDB重写是指bgsave、aof是压缩)都会触发fork操作，也就是父进程创建一个新的进程，这是一个比较耗时的过程，
可以使用info stats查看latest_fork_usec指标。
2. AOF刷盘阻塞  
开启AOF后，一般每秒将缓冲区的内容持久化到文件中，如果硬盘压力大，fsync操作将会阻塞。
可以通过info persistence统计aof_delayed_fsync指标
3. HugePage写操作阻塞

## 外在原因

1. CPU竞争
2. 内存交换
3. 网络问题
    + 网络闪断
    + 超过连接限制
    + 网络延迟