---
title: 六、redis内存模型
date: 2020-12-02 21:12:14
categories: redis专题
tags: 
    - 内存
description: redis基础介绍，包括概念以及使用方法
---

##  内存消耗统计

Redis进程内存占用主要可以划分为以下几个部分：

1. 自身内存，也就是程序的内存，一般来说非常小，可能只有几MB
2. 对象内存，也就是存储的数据，采用key-value的方式，每次至少创建两个类型对象。
3. 缓冲内存，各种缓冲区，客户端缓冲（输入输出）、复制积压缓冲区、AOF缓冲区
4. 内存碎片：used_memory_rss - used_memory,内存碎片是Redis在分配、回收物理内存过程中产生的。内存碎片产生原因主要是对数据的频繁修改造成，导致Redis释放的空间在物理内存中并没有被释放。在处理数据时，应该尽量数据对齐，防止大小不均。若Redis服务器中内存碎片很大，可以通过安全重启方式释放内存。

![内存划分](https://upload-images.jianshu.io/upload_images/23765437-776baaed9b4800fe.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


Redis自带一个内存统计的命令`info memory`，可以查看内存相关指标，每个指标都有两个值，一个是以字节为单位，一个是可读方式，也就是M或者G等，这里列举几个重要的且可读的指标：

+ used_memory:1.85M 内存总量，也就是数据占用的内存
+ used_memory_rss:13.94M 占用物理内存总量
+ used_memory_peak:1.89M 内存使用的最大值
+ used_memory_peak_perc:97.94% 
+ mem_fragmentation_ratio:7.68  used_memory_rss/used_memory 代表内存碎片率
+ used_memory_dataset:19334 数据集

## 子进程内存消耗

子进程内存消耗主要在执行AOF/RDB重写时Redis创建的子进程内存消耗。Linux具有写时复制技术(copy-on-write),也就是使用内存快照，共享物理内存页，父进程只是对修改复制一份副本。
>Linux 2.6.38增加了Transparent Huge Pages(THP)机制。开启与关闭该功能，对子进程内存消耗影响巨大。

## 内存管理

1. 设置内存上限：
    + 当超出内存上限时，会触发LRU等策略释放内存。
    + 防止内存超出物理内存，注意设置的是used_memory的量，并不包括碎片占用的
2. 动态调整内存上限

3. 内存回收策略  
Redis每个键都可以设置过期属性，但由于保留了大量的键，维护每个键消耗太大，几乎不可能，因此redis采用的是惰性删除和定时任务删除机制来实现过期键的回收。
    + 惰性删除：当客户端访问一个键，redis这时会判断它是否过期，如果过期就删除并返回空。
    + 定时任务删除：默认每秒运行10次，采用自适应算法，根据键的过期比例采用快慢两种模式回收，

4. 内存溢出控制策略，受参数maxmemory-policy控制
    + noeviction：默认策略，不删除数据，但拒绝写入并返回错误信息
    + volatile-lru: 根据LRU算法删除键，直到腾出足够空间
    + allkeys-random: 随机删除所有键，直到腾出足够空间
    + volatile-random：随机删除过期键，直到腾出空间
    + volatile-ttl: 根据对象ttl属性，删除最近将要过期的数据，如果没有会退到noeviction